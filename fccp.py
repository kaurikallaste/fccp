import configparser

config = configparser.ConfigParser()
config.read("config.ini")

file_path = config["CONFIG"]["FileLocation"]
text_file_location = config["CONFIG"]["TextLocation"]
f = 0
text_file_data = ""

def main():
    #ava fail kus on tekst mida kirjutada
    try:
        f = open(text_file_location, "r+")
        text_file_data = f.read()
    except FileNotFoundError:
        print("text file was not found")
        return 0

    #ava fail kui faili ei ole loo fail
    try:
        f = open(file_path, "r+")
        read_data = f.read()
        if read_data == text_file_data:
            #fail on olemas ja õige sisuga midagi ei ole vaja teha
            return 1
        else:
            #faili sisu on vale
            f.truncate(0)
            f.write(text_file_data)
            return 1
    except FileNotFoundError:
        #fail ei olnud olemas
        file = open(file_path, "w")
        f.write(text_file_data)
        return 1

if __name__ == "__main__":
    main()
